import migrate from './lib/migrate.js'
import { disconnect } from '#lib/server/db'
import logger from '#lib/server/logger'

await migrate()

logger.info('migrations successfully applied.')

await disconnect()
