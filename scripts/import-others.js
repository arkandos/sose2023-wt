import { createFromCsv } from '#lib/server/models/other'
import importCsvCli from './lib/import-csv.js'

await importCsvCli({
	tableName: 'other',

	columns: ['post_id', 'user_id', 'text', 'created'],

	createFromCsv
})
