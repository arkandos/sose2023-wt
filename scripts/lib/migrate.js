import { fileURLToPath } from 'url'
import fs from 'fs'
import path from 'path'
import { applyMigrations } from '#lib/server/db'

export default async function migrate() {
	const migrationDir = path.join(path.dirname(fileURLToPath(import.meta.url)), '../../migrations')
	const filenames = fs.readdirSync(migrationDir)
	const migrations = Object.fromEntries(
		filenames.map((filename) => [
			filename,
			fs.readFileSync(path.join(migrationDir, filename), { encoding: 'utf-8' })
		])
	)

	return await applyMigrations(migrations)
}
