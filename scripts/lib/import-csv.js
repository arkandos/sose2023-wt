import fs from 'node:fs'
import * as csvparse from 'csv-parse'
import minimist from 'minimist'

import { disconnect, fixAutoIncrement, truncate, useTransaction } from '#lib/server/db'
import logger from '#lib/server/logger'
import migrate from './migrate.js'

export default async function importCsvCli({ tableName, columns, createFromCsv }) {
	const args = minimist(process.argv.slice(2))

	const filename = args._[0]
	if (!filename) {
		console.error('Usage:', process.argv[0], process.argv[1], '[--truncate]', '<filename.csv>')
		process.exit(1)
	}

	await migrate()

	if (args.truncate) {
		await truncate(null, tableName)
	}

	const parserOptions = {
		cast: true,
		// cast_date: true,

		// new Date('decimalRice7') // --> 2001-12-07
		// javascript was, is, and will probably ever be a mistake.

		columns
	}
	const parser = fs.createReadStream(filename).pipe(csvparse.parse(parserOptions))

	for await (const row of parser) {
		logger.info(`creating ${tableName}: ${JSON.stringify(row)}`)

		await useTransaction(async (client) => {
			return await createFromCsv(client, row)
		})
	}

	await fixAutoIncrement(null, tableName)
	await disconnect()
}
