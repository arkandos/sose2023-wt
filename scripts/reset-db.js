import { disconnect, useClient, query } from '#lib/server/db'
import logger from '#lib/server/logger'
import migrate from './lib/migrate.js'

await useClient(async (client) => {
	await query(client, 'DROP SCHEMA IF EXISTS public CASCADE;')
	await query(client, 'CREATE SCHEMA IF NOT EXISTS public;')
})

logger.info('public schema dropped & re-created.')

await migrate()

logger.info('migrations successfully applied.')

await disconnect()
