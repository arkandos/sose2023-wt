import { createFromCsv } from '#lib/server/models/othler'
import importCsvCli from './lib/import-csv.js'

await importCsvCli({
	tableName: 'othler',

	columns: ['user_id', 'name', 'password', 'birthday', 'profile_pic', 'bio_text', 'created'],

	createFromCsv
})
