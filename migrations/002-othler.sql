CREATE TABLE othler (
    id SERIAL PRIMARY KEY,

    handle VARCHAR(50) NOT NULL UNIQUE,
    password_hash VARCHAR(255) NOT NULL,

    "name" VARCHAR(50) NOT NULL,
    bio_markdown TEXT NOT NULL,
    bio_html TEXT NOT NULL,
    birthday DATE DEFAULT NULL,

    avatar INTEGER REFERENCES embed(id) ON DELETE RESTRICT DEFAULT NULL,
    banner INTEGER REFERENCES embed(id) ON DELETE RESTRICT DEFAULT NULL,

    -- de-normalized counts, managed by triggers
    posts_count INTEGER NOT NULL DEFAULT 0, -- SELECT COUNT(*) FROM other WHERE othler_id = $1
    following_count INTEGER NOT NULL DEFAULT 0, -- SELECT COUNT(*) FROM othler_following WHERE othler_id = $1
    followed_count INTEGER NOT NULL DEFAULT 0, -- SELECT COUNT(*) FROM othler_following WHERE follows_id = $1

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- users listing
CREATE INDEX othler_created_at ON othler (created_at);

-- When deleting a othler, clean up the embeds.
CREATE FUNCTION othler_on_delete() RETURNS TRIGGER AS $$
BEGIN
    IF OLD.avatar IS NOT NULL THEN
        DELETE FROM embed WHERE id = OLD.avatar;
    END IF;
    IF OLD.banner IS NOT NULL THEN
        DELETE FROM embed WHERE id = OLD.banner;
    END IF;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_othler_on_delete AFTER DELETE ON othler FOR EACH ROW EXECUTE FUNCTION othler_on_delete();


CREATE TABLE othler_fields (
    othler_id INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,
    position INTEGER NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "value" TEXT NOT NULL,
    PRIMARY KEY (othler_id, position)
);

CREATE TABLE othler_following (
    othler_id INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,
    follows_id INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (othler_id, follows_id)
);

-- bi-directional following/followed by graph
CREATE UNIQUE INDEX othler_followed ON othler_following (follows_id, othler_id);

-- automatically update following_count, followed_count
CREATE FUNCTION othler_following_create() RETURNS TRIGGER AS $$
BEGIN
    UPDATE othler SET following_count = following_count + 1 WHERE id = NEW.othler_id;
    UPDATE othler SET followed_count = followed_count + 1 WHERE id = NEW.follows_id;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_othler_following_create AFTER INSERT ON othler_following FOR EACH ROW EXECUTE FUNCTION othler_following_create();

CREATE FUNCTION othler_following_delete() RETURNS TRIGGER AS $$
BEGIN
    UPDATE othler SET following_count = following_count - 1 WHERE id = OLD.othler_id;
    UPDATE othler SET followed_count = followed_count - 1 WHERE id = OLD.follows_id;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_othler_following_delete AFTER DELETE ON othler_following FOR EACH ROW EXECUTE FUNCTION othler_following_delete();
