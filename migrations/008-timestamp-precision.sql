-- Javascript only supports millisecond precision in Date objects,
-- while Postgres uses microseconds by default.

-- We would REALLY like for the round-trip to work though, to make sure
-- equality is well-defined on our timestamp columns, such that we can
-- properly sort and search for those values.
-- (equality is important for our pagination scheme)

-- We also need to re-create the view because postgres...
-- this trick was taken from https://stackoverflow.com/a/62793792

do $$
    declare view_othler_def text;
    declare view_othler_sql text;
begin

    view_othler_def := pg_get_viewdef('view_othler');
    DROP VIEW view_othler;

    ALTER TABLE othler ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;
    ALTER TABLE othler ALTER COLUMN updated_at TYPE TIMESTAMP(3) USING updated_at;

    ALTER TABLE othler_following ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;

    ALTER TABLE other ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;
    ALTER TABLE other ALTER COLUMN updated_at TYPE TIMESTAMP(3) USING updated_at;

    ALTER TABLE other_embed ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;

    ALTER TABLE other_likes ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;

    ALTER TABLE other_mentions ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;

    ALTER TABLE other_hashtags ALTER COLUMN created_at TYPE TIMESTAMP(3) USING created_at;

    view_othler_sql := format('CREATE VIEW view_othler AS %s', view_othler_def);
    EXECUTE view_othler_sql;
end $$;
