-- TIL: MySQL famously truncates automically, but it seems like Postgres doesn't and correctly fails insteaD?

ALTER TABLE embed DROP CONSTRAINT constraint_embed_name_length;
ALTER TABLE othler DROP CONSTRAINT constraint_othler_handle_length;
ALTER TABLE othler DROP CONSTRAINT constraint_othler_name_length;
ALTER TABLE othler_fields DROP CONSTRAINT constraint_othler_fields_name;
