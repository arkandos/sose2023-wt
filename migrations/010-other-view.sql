CREATE VIEW view_other AS (
    WITH other_single_level AS (
        SELECT
            other.*,

            (SELECT
                jsonb_build_object(
                'id', othler.id,
                'handle', othler.handle,
                'name', othler.name,
                'avatar',
                    CASE WHEN avatar.id IS NOT NULL
                    THEN jsonb_build_object(
                        'id', avatar.id,
                        'name', avatar.name
                    )
                    ELSE NULL
                    END

                )
                FROM othler
                LEFT JOIN embed avatar ON avatar.id = othler.avatar
                WHERE othler.id = other.othler_id
            ) AS othler,

            COALESCE((
                SELECT jsonb_agg(jsonb_build_object(
                'id', e.id,
                'name', e.name
                ))
                FROM other_embed pe
                INNER JOIN embed e ON pe.embed_id = e.id
                WHERE pe.other_id = other.id
            ), '[]'::jsonb) AS embeds,

            (
                SELECT to_jsonb(preview.*)
                FROM other_preview preview
                WHERE preview.other_id = other.id
            ) AS preview
        FROM other
    )
    SELECT
        other.id,
        other.othler,

        other.embeds,
        other.preview,
        other.text_markdown,
        other.text_html,

        to_jsonb(reply.*) AS in_reply_to,
        to_jsonb(repost.*) as reposts,

        other.repost_count,
        other.likes_count,
        other.replies_count,

        other.created_at,
        other.updated_at
    FROM other_single_level other
    LEFT JOIN other_single_level reply ON reply.id = other.in_reply_to
    LEFT JOIN other_single_level repost ON repost.id = other.reposts
)
