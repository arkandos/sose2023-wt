CREATE FUNCTION othler_on_update() RETURNS TRIGGER AS $$
BEGIN
    IF OLD.avatar IS NOT NULL AND OLD.avatar != NEW.avatar THEN
        DELETE FROM embed WHERE id = OLD.avatar;
    END IF;
    IF OLD.banner IS NOT NULL AND OLD.banner != NEW.banner THEN
        DELETE FROM embed WHERE id = OLD.banner;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_othler_on_update AFTER UPDATE ON othler FOR EACH ROW EXECUTE FUNCTION othler_on_update();
