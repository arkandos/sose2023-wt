CREATE TABLE other (
    id SERIAL PRIMARY KEY,
    othler_id INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,

    text_markdown TEXT NOT NULL,
    text_html TEXT NOT NULL,

    -- TODO: Do we really want to delete the entire history if a
    -- parent post gets deleted?
    in_reply_to INTEGER REFERENCES other(id) ON DELETE CASCADE DEFAULT NULL,

    -- reposting creates a new post, since that is easiest to handle.
    reposts INTEGER REFERENCES other(id) ON DELETE CASCADE DEFAULT NULL,

    -- de-normalize counts such that we do not have to run count queries all the time.
    -- managed by triggers
    repost_count INTEGER NOT NULL DEFAULT 0, -- SELECT COUNT(*) FROM other WHERE reposts = $1
    likes_count INTEGER NOT NULL DEFAULT 0, -- SELECT COUNT(*) FROM other_likes WHERE other_id = $1
    replies_count INTEGER NOT NULL DEFAULT 0, -- SELECT COUNT(*) FROM other WHERE in_reply_to = $1

    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- global timeline
CREATE INDEX other_created_at ON other (created_at DESC);
-- posts by othler, to construct the user timeline
CREATE INDEX other_othler ON other (othler_id, created_at DESC);
-- just posts without reposts and replies, for the user page
CREATE INDEX other_othler_posts ON other (othler_id, created_at DESC) WHERE reposts IS NULL AND in_reply_to IS NULL;
-- just posts without reposts but with replies, for the user page
CREATE INDEX other_othler_posts_and_replies ON other (othler_id, created_at DESC) WHERE reposts IS NULL;
-- posts by in_reply_to, to list replies
CREATE INDEX other_replies ON other (in_reply_to, created_at DESC);
-- additional cascade
CREATE INDEX other_reposts ON other (reposts);


-- automatically update repost/reply counts, post count on othler
CREATE FUNCTION other_create() RETURNS TRIGGER AS $$
BEGIN
    UPDATE othler SET posts_count = posts_count + 1 WHERE id = NEW.othler_id;

    IF NEW.in_reply_to IS NOT NULL THEN
        UPDATE other SET replies_count = replies_count + 1 WHERE id = NEW.in_reply_to;
    END IF;
    IF NEW.reposts IS NOT NULL THEN
        UPDATE other SET repost_count = repost_count + 1 WHERE id = NEW.reposts;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_other_create AFTER INSERT ON other FOR EACH ROW EXECUTE FUNCTION other_create();


CREATE FUNCTION other_delete() RETURNS TRIGGER AS $$
BEGIN
    UPDATE othler SET posts_count = posts_count - 1 WHERE id = OLD.othler_id;

    IF OLD.in_reply_to IS NOT NULL THEN
        UPDATE other SET replies_count = replies_count - 1 WHERE id = OLD.in_reply_to;
    END IF;
    IF OLD.reposts IS NOT NULL THEN
        UPDATE other SET repost_count = repost_count - 1 WHERE id = OLD.reposts;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_other_delete AFTER DELETE ON other FOR EACH ROW EXECUTE FUNCTION other_delete();


CREATE TABLE other_embed (
    other_id INTEGER NOT NULL REFERENCES other(id) ON DELETE CASCADE,
    othler_id INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,
    embed_id INTEGER NOT NULL REFERENCES embed(id) ON DELETE RESTRICT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (other_id, embed_id)
);

-- media listing on user page
CREATE INDEX other_embed_othler ON other_embed (othler_id, created_at DESC);

-- When deleting an other, the delete gets cascaded to other_embed, so if we delete an other_embed,
-- we clean up the embed afterwards.
CREATE FUNCTION other_embed_delete() RETURNS TRIGGER AS $$
BEGIN
    DELETE FROM embed WHERE id = OLD.embed_id;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_other_embed_delete AFTER DELETE ON other_embed FOR EACH ROW EXECUTE FUNCTION other_embed_delete();

CREATE TABLE other_preview (
    id SERIAL PRIMARY KEY,
    other_id INTEGER REFERENCES other(id) ON DELETE CASCADE,
    "url" TEXT NOT NULL,
    title TEXT NOT NULL,
    summary TEXT NOT NULL,
    icon_url TEXT DEFAULT NULL
);

CREATE INDEX other_preview_other ON other_preview (other_id);


CREATE TABLE other_likes (
    other_id INTEGER NOT NULL REFERENCES other(id) ON DELETE CASCADE,
    liked_by INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (other_id, liked_by)
);

-- posts that I liked
CREATE UNIQUE INDEX other_liked ON other_likes (liked_by, created_at DESC);

-- automatically update likes counts
CREATE FUNCTION other_likes_create() RETURNS TRIGGER AS $$
BEGIN
    UPDATE other SET likes_count = likes_count + 1 WHERE id = NEW.other_id;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_other_likes_create AFTER INSERT ON other_likes FOR EACH ROW EXECUTE FUNCTION other_likes_create();

CREATE FUNCTION other_likes_delete() RETURNS TRIGGER AS $$
BEGIN
    UPDATE other SET likes_count = likes_count - 1 WHERE id = OLD.other_id;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER trigger_other_likes_delete AFTER DELETE ON other_likes FOR EACH ROW EXECUTE FUNCTION other_likes_delete();


-- @mentions
CREATE TABLE other_mentions (
    other_id INTEGER NOT NULL REFERENCES other(id) ON DELETE CASCADE,
    othler_id INTEGER NOT NULL REFERENCES othler(id) ON DELETE CASCADE,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(other_id, othler_id)
);

-- get mentions for user
CREATE UNIQUE INDEX other_mentioned ON other_mentions (othler_id, other_id);

-- #hashtags
CREATE TABLE other_hashtags (
    hashtag VARCHAR(50) NOT NULL,
    other_id INTEGER NOT NULL REFERENCES other(id) ON DELETE CASCADE,
    created_at TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(other_id, hashtag)
);

-- trends: SELECT hashtag, COUNT(*) FROM hashtag GROUP BY hashtag WHERE created_at > CURRENT_TIMESTAMP - 3 DAYS
CREATE INDEX other_trends ON other_hashtags (hashtag, created_at);



