DROP VIEW view_other;

ALTER TABLE other ADD COLUMN embed_count INTEGER NOT NULL DEFAULT 0;

ALTER TABLE othler ADD COLUMN embed_count INTEGER NOT NULL DEFAULT 0;

UPDATE other SET embed_count = (SELECT COUNT(e.*) FROM other_embed e WHERE e.other_id = other.id);

UPDATE othler
SET
    -- FIXME: update counts on othler update!
    embed_count =
        (SELECT COUNT(e.*) FROM other_embed e WHERE e.othler_id = id)
        + (CASE WHEN avatar IS NOT NULL THEN 1 ELSE 0 END)
        + (CASE WHEN banner IS NOT NULL THEN 1 ELSE 0 END)
;

CREATE OR REPLACE FUNCTION other_embed_delete() RETURNS TRIGGER AS $$
BEGIN
    UPDATE other SET embed_count = embed_count - 1 WHERE id = OLD.other_id;
    UPDATE othler SET embed_count = embed_count - 1 WHERE id = OLD.othler_id;
    DELETE FROM embed WHERE id = OLD.embed_id;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION other_embed_create() RETURNS TRIGGER AS $$
BEGIN
    UPDATE other SET embed_count = embed_count + 1 WHERE id = NEW.other_id;
    UPDATE othler SET embed_count = embed_count + 1 WHERE id = NEW.othler_id;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_other_embed_create AFTER INSERT ON other_embed FOR EACH ROW EXECUTE FUNCTION other_embed_create();

CREATE INDEX other_othler_embeds ON other (othler_id, embed_count, created_at DESC, id DESC);

CREATE VIEW view_other AS (
    WITH other_single_level AS (
        SELECT
            other.*,

            (SELECT
                jsonb_build_object(
                'id', othler.id,
                'handle', othler.handle,
                'name', othler.name,
                'avatar',
                    CASE WHEN avatar.id IS NOT NULL
                    THEN jsonb_build_object(
                        'id', avatar.id,
                        'name', avatar.name
                    )
                    ELSE NULL
                    END

                )
                FROM othler
                LEFT JOIN embed avatar ON avatar.id = othler.avatar
                WHERE othler.id = other.othler_id
            ) AS othler,

            COALESCE((
                SELECT jsonb_agg(jsonb_build_object(
                'id', e.id,
                'name', e.name
                ))
                FROM other_embed pe
                INNER JOIN embed e ON pe.embed_id = e.id
                WHERE pe.other_id = other.id
            ), '[]'::jsonb) AS embeds,

            (
                SELECT to_jsonb(preview.*)
                FROM other_preview preview
                WHERE preview.other_id = other.id
            ) AS preview
        FROM other
    )
    SELECT
        other.id,

        other.othler_id,
        other.othler,

        other.embeds,
        other.preview,
        other.text_markdown,
        other.text_html,

        other.in_reply_to AS in_reply_to_id,
        to_jsonb(reply.*) AS in_reply_to,

        other.reposts AS reposts_id,
        to_jsonb(repost.*) AS  reposts,

        other.embed_count,
        other.repost_count,
        other.likes_count,
        other.replies_count,

        other.created_at,
        other.updated_at
    FROM other_single_level other
    LEFT JOIN other_single_level reply ON reply.id = other.in_reply_to
    LEFT JOIN other_single_level repost ON repost.id = other.reposts
)
