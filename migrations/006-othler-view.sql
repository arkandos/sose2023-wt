CREATE VIEW view_othler AS
SELECT
    o.id,
    o.handle,

    o.name,
    o.bio_markdown,
    o.bio_html,

    o.birthday,

    o.posts_count,
    o.following_count,
    o.followed_count,

    (
        SELECT jsonb_build_object(
            'id', a.id,
            'name', a.name
        ) FROM embed a WHERE a.id = o.avatar
    ) AS avatar,

    (
        SELECT jsonb_build_object(
            'id', b.id,
            'name', b.name
        ) FROM embed b WHERE b.id = o.banner
    ) AS banner,

    COALESCE((
        SELECT jsonb_agg(f.*) FROM othler_fields f WHERE f.othler_id = o.id
    ), '[]'::jsonb) AS fields,

    o.created_at,
    o.updated_at
FROM othler o;
