-- used by the "reposts" feature to make sure there is only ever 1 repost per othler
CREATE UNIQUE INDEX other_othler_reposts ON other (othler_id, reposts);
