
-- global timeline
CREATE INDEX other_pagination ON other (created_at DESC, id DESC);
-- posts by othler, to construct the user timeline
DROP INDEX other_othler;
CREATE INDEX other_othler ON other (othler_id, created_at DESC, id DESC);
-- just posts without reposts and replies, for the user page
DROP INDEX other_othler_posts;
CREATE INDEX other_othler_posts ON other (othler_id, created_at DESC, id DESC) WHERE reposts IS NULL AND in_reply_to IS NULL;
-- just posts without reposts but with replies, for the user page
DROP INDEX other_othler_posts_and_replies;
CREATE INDEX other_othler_posts_and_replies ON other (othler_id, created_at DESC, id DESC) WHERE reposts IS NULL;
-- posts by in_reply_to, to list replies
DROP INDEX other_replies;
CREATE INDEX other_replies ON other (in_reply_to, created_at DESC, id DESC);

