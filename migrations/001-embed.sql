CREATE TABLE embed (
    id SERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "local" BOOLEAN NOT NULL,
    "data" BYTEA
);

-- use ON DELETE RESTRICT on references to embed, since we cannot
-- delete an embed without breaking posts/users.
-- the embed is the "parent" table in this case.

-- add a trigger that deletes the embed object after deleting the referencing owner.
