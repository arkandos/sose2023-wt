ALTER TABLE embed ADD CONSTRAINT constraint_embed_name_length CHECK (length(name) <= 255);

ALTER TABLE othler ADD CONSTRAINT constraint_othler_handle_length CHECK (length(handle) <= 50);
ALTER TABLE othler ADD CONSTRAINT constraint_othler_handle_pattern CHECK (handle ~ '^[a-zA-Z0-9_]+$');
ALTER TABLE othler ADD CONSTRAINT constraint_othler_name_length CHECK (length(name) <= 50);

ALTER TABLE othler_fields ADD CONSTRAINT constraint_othler_fields_name CHECK (length(name) <= 255);

-- DO NOT CHECK hashtags, we just collapse them.
