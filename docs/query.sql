-- our timeline query will probably look like this:

-- SELECT other.*
-- FROM other
-- WHERE
       -- own posts
--     other.othler_id = $1 OR
       -- posts from followed people
--     other.othler_id IN (SELECT follows_id FROM othler_following WHERE othler_id = $1) OR
       -- posts where I was mentioned
--     other.id IN (SELECT other_id FROM other_mentions WHERE othler_id = $1)
-- ORDER BY created_at DESC

-- we might instead also do this:
-- SELECT * FROM other
-- WHERE id IN (
--     SELECT id FROM other WHERE othler_id = $1
--     UNION SELECT id FROM other WHERE othler_id IN (SELECT follows_id FROM othler_following WHERE othler_id = $1)
--     UNION SELECT other_id FROM other_mentions WHERE othler_id = $1
-- )
-- ORDER BY id DESC

-- this will give us duplicate posts in the UI if multiple people we follow
-- repost/reply to the same post.
-- we will deduplicate those in the UI, since I can't figure out a good way to put this into the select
