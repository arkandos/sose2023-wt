<?php
class AutoLogin {
    function __construct() {
        if (
            $_SESSION["pwds"][$_ENV['AUTOLOGIN_DB_DRIVER']][$_ENV['ADMINER_DEFAULT_SERVER']][$_ENV['AUTOLOGIN_DB_USER']] !== $_ENV['AUTOLOGIN_DB_PASS']
            && $_SESSION["db"][$_ENV['AUTOLOGIN_DB_DRIVER']][$_ENV['ADMINER_DEFAULT_SERVER']][$_ENV['AUTOLOGIN_DB_USER']][$_ENV['AUTOLOGIN_DB_PASS']] !== true
            || isset($_POST["auth"])
        ) {
            // If the current session doesn't have a valid connection details
            // (so you're not logged in) or the login form was submitted,
            // make Adminer think that you just submitted the login form
            // with the correct connection details.
            $_POST["auth"]["server"] = $_ENV['ADMINER_DEFAULT_SERVER'];
            $_POST["auth"]["driver"] = $_ENV['AUTOLOGIN_DB_DRIVER'];
            $_POST["auth"]["db"] = $_ENV['AUTOLOGIN_DB_NAME'];
            $_POST["auth"]["username"] = $_ENV['AUTOLOGIN_DB_USER'];
            $_POST["auth"]["password"] = $_ENV['AUTOLOGIN_DB_PASS'];
            $_POST["auth"]["permanent"] = 1;
        } else {
            // If logged in and on the home page redirect to the currently
            // logged in database server page.
            if (
                $_GET[$_ENV['AUTOLOGIN_DB_DRIVER']] !== $_ENV['ADMINER_DEFAULT_SERVER']
                || $_GET['username'] !== $_ENV['AUTOLOGIN_DB_USER']
            ) {
                $query = $_ENV['AUTOLOGIN_DB_DRIVER'] . '=' . $_ENV['ADMINER_DEFAULT_SERVER'] . '&username=' . $_ENV['AUTOLOGIN_DB_USER'];
                header('Location: /?'. $query);
            }
        }
    }
}

return new AutoLogin();
