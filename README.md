# OTHler

Ein Twitter-Klon.

## Quick start

Bitte `npm run korrektur` ausführen (nach `npm i`).

Das baut ein Release, und führt dieses dann mit den passenden Umgebungsvariablen aus.

## 500 - Not found

Wenn dieser Fehler erscheint, ist eventuell noch ein Cookie "auth" vorhanden, der nicht zu dieser Anwendung gehört, oder der `APP_AUTH_KEY` hat sich geändert.

## Tech-Stack:

- SvelteKit Fullstack (Svelte 3)
- Bootstrap (5.2)
- Postgres
- Marked, Katex, Cheerio, Insane for content handling
