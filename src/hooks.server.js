import { building } from '$app/environment'
import { disconnect, applyMigrations } from '#lib/server/db'
import { _handler } from './routes/api/auth/+server'

if (!building) {
	const migrations = import.meta.glob('../migrations/*.sql', {
		as: 'raw',
		eager: true
	})

	await applyMigrations(migrations)

	process.once('SIGINT', async () => {
		await disconnect()
		process.exit(0)
	})
}

export const handle = _handler
