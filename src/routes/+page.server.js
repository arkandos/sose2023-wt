import { redirect } from '@sveltejs/kit'

export function load({ cookies }) {
	if (cookies?.get('auth')) {
		throw redirect(307, '/home')
	}
}
