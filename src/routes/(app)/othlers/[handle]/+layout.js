import createApiClient from '#lib/api'

export async function load({ params, fetch }) {
	const api = createApiClient({ fetch })
	const othler2 = await api.get('/othlers/' + params.handle)

	return {
		othler2
	}
}
