import createApiClient from '#lib/api'

export async function load({ params, fetch }) {
	const api = createApiClient({ fetch })
	const other = await api.get('/others/' + params.id)

	return {
		other
	}
}
