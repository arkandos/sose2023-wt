import createApiClient from '#lib/api'

export const ssr = false

export async function load({ fetch }) {
	const client = createApiClient({ fetch })

	const [othler, { items: trends }] = await Promise.all([client.get('/me'), client.get('/tags')])

	return { othler, trends }
}
