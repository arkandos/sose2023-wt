import * as Embed from '#lib/server/models/embed'

export async function GET({ params }) {
	const { id } = params

	const embed = await Embed.getById(null, id)

	const headers = new Headers()
	headers.append('Cache-Control', 'public, max-age=31536000, immutable')
	headers.append('Content-Type', embed.mime)
	if (embed.size) {
		headers.append('Content-Length', embed.size)
	}

	return new Response(embed.data, { headers })
}
