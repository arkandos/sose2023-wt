import { json } from '@sveltejs/kit'
import * as Other from '#lib/server/models/other'

export async function PUT({ params, locals }) {
	const { othler } = locals
	const { id } = params

	const ok = await Other.like(null, othler.id, id)
	return json({ ok })
}

export async function DELETE({ params, locals }) {
	const { othler } = locals
	const { id } = params

	const ok = await Other.unlike(null, othler.id, id)
	return json({ ok })
}
