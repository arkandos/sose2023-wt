import { json, error } from '@sveltejs/kit'
import * as Other from '#lib/server/models/other'

export async function GET({ locals, params }) {
	const { othler } = locals
	const { id } = params

	const other = await Other.getById(null, othler.id, id)
	if (!other) {
		throw error(404)
	}

	return json(other)
}
