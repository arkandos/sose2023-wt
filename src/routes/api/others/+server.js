import { json } from '@sveltejs/kit'
import * as Other from '#lib/server/models/other'
import { getOthlerPaginated } from '#lib/server/controllers/othler'
import { useTransaction } from '#lib/server/db'

export const GET = getOthlerPaginated((client, selfId, othlerId, limit, lastKey) =>
	Other.getLocalTimeline(client, limit, lastKey)
)

export async function POST({ locals, request }) {
	const { othler } = locals

	const formData = await request.formData()
	const text = formData.get('text')
	const files = formData.getAll('files')

	const other = await useTransaction((client) => Other.post(client, othler.id, text, files))

	return json(other)
}
