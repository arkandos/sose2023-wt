import { json } from '@sveltejs/kit'
import * as Othler from '#lib/server/models/othler'
import { useTransaction } from '#lib/server/db'

export function GET({ locals }) {
	return json(locals.othler)
}

export async function PUT({ locals, request }) {
	const { othler } = locals

	const formData = await request.formData()
	const data = JSON.parse(formData.get('data'))
	// as per spec, FormData.prototype.get returns null if the key is not found.
	// we need undefined though.
	const avatar = formData.get('avatar') ?? undefined
	const banner = formData.get('banner') ?? undefined

	await useTransaction((client) =>
		Othler.update(client, othler.id, {
			avatar,
			banner,
			name: data.name,
			bio_markdown: data.bio_markdown,
			birthday: data.birthday,
			fields: data.fields
		})
	)

	const newOthler = await Othler.getSelf(null, othler.id)
	return json(newOthler)
}
