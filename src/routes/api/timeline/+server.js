import * as Other from '#lib/server/models/other'
import { getOthlerPaginated } from '#lib/server/controllers/othler'

export const GET = getOthlerPaginated((client, selfId, othlerId, limit, lastKey) =>
	Other.getTimeline(client, selfId, limit, lastKey)
)
