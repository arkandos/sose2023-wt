import * as Othler from '#lib/server/models/othler'
import { getOthlerPaginated } from '#lib/server/controllers/othler'

export const GET = getOthlerPaginated((client, selfId, othlerId, limit, lastKey) =>
	Othler.getRegistered(client, selfId, limit, lastKey)
)
