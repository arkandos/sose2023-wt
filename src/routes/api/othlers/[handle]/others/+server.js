import { getOthlerPaginated } from '#lib/server/controllers/othler'
import * as Other from '#lib/server/models/other'

export const GET = getOthlerPaginated(Other.getOthlerPosts)
