import { getOthlerPaginated } from '#lib/server/controllers/othler'
import * as Othler from '#lib/server/models/othler'

export const GET = getOthlerPaginated(Othler.getFollowing)
