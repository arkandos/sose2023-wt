import { json } from '@sveltejs/kit'
import { withHandle, getOthlerPaginated } from '#lib/server/controllers/othler'
import * as Othler from '#lib/server/models/othler'

export const GET = getOthlerPaginated(Othler.getFollowers)

export const PUT = withHandle(async ({ locals }) => {
	const followerId = locals.othler.id
	const followingId = locals.handleId
	await Othler.follow(null, followerId, followingId)
	return json({ ok: true })
})

export const DELETE = withHandle(async ({ locals }) => {
	const followerId = locals.othler.id
	const followingId = locals.handleId
	await Othler.unfollow(null, followerId, followingId)
	return json({ ok: true })
})
