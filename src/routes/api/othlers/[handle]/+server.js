import { error, json } from '@sveltejs/kit'
import * as Othler from '#lib/server/models/othler'

export async function GET({ locals, params }) {
	const { othler: self } = locals
	const { handle } = params

	const othler = await Othler.getByHandle(null, self.id, handle)
	if (!othler) {
		throw error(404)
	}

	return json(othler)
}
