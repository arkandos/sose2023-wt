import { json } from '@sveltejs/kit'
import * as Other from '#lib/server/models/other'

const trendsSince = 30 * 24 * 60 * 60 * 1000 // 30 days

export async function GET() {
	const since = new Date(Date.now() - trendsSince)
	const trends = await Other.getTrends(null, since, 4)
	return json({ items: trends })
}
