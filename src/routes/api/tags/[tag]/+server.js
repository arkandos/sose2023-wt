import { json } from '@sveltejs/kit'
import * as Others from '#lib/server/models/other'

export async function GET({ locals, params, url }) {
	const { othler } = locals
	const { tag } = params

	const lastKey = url.searchParams.size ? Object.fromEntries(url.searchParams) : null

	const { items, next } = await Others.getByTag(null, othler.id, tag, 10, lastKey)
	return json({ items, next })
}
