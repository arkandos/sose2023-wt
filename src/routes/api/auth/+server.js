import { randomBytes } from 'node:crypto'
import jwt from 'jsonwebtoken'

import { error, json } from '@sveltejs/kit'
import logger from '#lib/server/logger'
import * as Othler from '#lib/server/models/othler'

const cookieName = 'auth'
const tokenMaxAge = 365 * 24 * 60 * 60 * 1000
const tokenSecret = process.env.APP_AUTH_SECRET || randomBytes(256).toString('base64')
const tokenAlgorithm = 'HS256'

export async function _handler({ event, resolve }) {
	const { cookies, url } = event

	// routes not starting with /api are svelte routes and always public
	if (!url.pathname.startsWith('/api')) {
		return await resolve(event)
	}

	// routes starting with /auth are blessed and check their
	// authentication/authorization needs themselves
	if (url.pathname === '/api/auth') {
		return await resolve(event)
	}

	const token = cookies.get(cookieName)
	if (!token) {
		logger.debug('no token, ' + url.pathname)
		throw error(401)
	}

	try {
		const decoded = jwt.verify(token, tokenSecret, {
			algorithms: [tokenAlgorithm]
		})

		const othler = await Othler.getSelf(null, decoded.sub)
		if (!othler) {
			logger.error('valid token, but user not found: ' + decoded.sub)
			throw error(401)
		}

		event.locals.othler = othler
	} catch (err) {
		logger.error('auth error: ' + err.stack)
		throw error(401)
	}

	return await resolve(event)
}

// profile exists?
export async function GET({ url }) {
	const handle = url.searchParams.get('handle')
	if (!handle) {
		throw error(404)
	}

	const name = await Othler.getNameByHandle(null, handle)
	if (!name) {
		throw error(404)
	}

	// we only want to expose the bare minimum publically.
	return json({ name })
}

// login
export async function PUT({ cookies, request }) {
	const { handle, password } = await request.json()

	const loginValid = await Othler.login(null, handle, password)
	if (!loginValid) {
		throw error(401)
	}

	const id = await Othler.getIdByHandle(null, handle)
	if (!id) {
		throw error(401) // shouldn't happen; how did the login succeed?
	}

	setLoginCookie(cookies, id)
	return json({ ok: true })
}

// logout
export async function DELETE({ cookies }) {
	cookies.delete(cookieName, { path: '/' })
	return json({ ok: true })
}

// register
export async function POST({ cookies, request }) {
	const { handle, name, password } = await request.json()

	const othler = await Othler.register(null, {
		handle,
		name,
		password
	})

	setLoginCookie(cookies, othler.id)
	return json({ ok: true })
}

function setLoginCookie(cookies, othlerId) {
	const token = jwt.sign({ sub: othlerId }, tokenSecret, {
		algorithm: tokenAlgorithm,
		expiresIn: tokenMaxAge + 'ms'
	})

	cookies.set(cookieName, token, {
		maxAge: tokenMaxAge / 1000,
		httpOnly: true,
		path: '/',
		sameSite: 'strict'
	})
}
