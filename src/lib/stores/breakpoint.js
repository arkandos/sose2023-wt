import { browser } from '$app/environment'
import { readable } from 'svelte/store'

// NOTE: Needs to be kept in sync with Bootstrap!
// NOTE: Needs to be in-order, since we return the last value!
const breakpoints = {
	sm: 576,
	md: 768,
	lg: 992,
	xl: 1200,
	xxl: 1400
}

export default readable('xs', (set) => {
	let matchers = {}

	const update = () => {
		let biggestBreakpoint = 'xs'
		for (const breakpoint in matchers) {
			if (matchers[breakpoint].matches) {
				biggestBreakpoint = breakpoint
			}
		}
		set(biggestBreakpoint)
	}

	const start = () => {
		if (!browser) {
			return
		}

		for (const breakpoint in breakpoints) {
			matchers[breakpoint] = window.matchMedia(`(min-width: ${breakpoints[breakpoint]}px)`)
			matchers[breakpoint].addListener(update)
		}
	}

	const stop = () => {
		for (const breakpoint in matchers) {
			matchers[breakpoint].removeListener(update)
		}
	}

	start()
	update()
	return stop
})
