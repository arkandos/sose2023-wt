import winston from 'winston'

const level = process.env.NODE_ENV === 'production' ? 'info' : 'debug'

const logger = winston.createLogger({
	level,
	format: winston.format.combine(
		winston.format.label({ label: 'activitypub-server' }),
		winston.format.timestamp(),
		// winston.format.prettyPrint()
		winston.format.printf(({ level, message, label, timestamp }) => {
			label // ignored
			return `${timestamp} [${level}]: ${message}`
		})
	),
	transports: [new winston.transports.Console()]
})

export default logger
