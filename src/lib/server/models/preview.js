import * as cheerio from 'cheerio'
import { insertObject } from '#lib/server/db'
import logger from '#lib/server/logger'

function text(selector) {
	return ($) => $(selector).text()
}

function attr(selector, attr) {
	return ($) => $(selector).attr(attr)
}

function metaProp(name) {
	return attr(`meta[property="${name}"]`, 'content')
}

function metaName(name) {
	return attr(`meta[name="${name}"]`, 'content')
}

function link(rel) {
	return attr(`link[rel="${rel}"]`, 'href')
}

function itemprop(prop) {
	return attr(`meta[itemprop="${prop}"]`, 'content')
}

const titleSelectors = [
	metaProp('og:title'),
	metaProp('twitter:title'),
	metaName('title'),
	itemprop('name'),
	text('title'),
	text('h1')
]

const summarySelectors = [
	metaProp('og:description'),
	metaProp('twitter:description'),
	metaName('description'),
	itemprop('description'),
	metaName('abstract'),
	metaName('subject')
]

const urlSelectors = [metaProp('og:url'), metaProp('twitter:url'), link('canonical')]

const imageSelectors = [
	metaProp('og:image'),
	metaProp('twitter:image'),
	metaProp('og:image:secure_url'),
	metaProp('og:image:url'),
	itemprop('image'),
	link('image_src'),
	metaName('thumbnail')
]

function firstMatch($, selectors, defaultValue = '') {
	for (const selector of selectors) {
		const result = selector($)
		if (result) {
			return result
		}
	}
	return defaultValue
}

export async function loadFromUrl(url) {
	// Cheerio's website is still a work-in-progress, and covers Cheerio's next release that isn't available yet
	// Ugh!
	// const $ = await cheerio.fromURL(url)

	const req = await fetch(url)
	const html = await req.text()

	const $ = cheerio.load(html)

	const title = firstMatch($, titleSelectors)
	const summary = firstMatch($, summarySelectors)
	const icon_url = firstMatch($, imageSelectors)
	url = firstMatch($, urlSelectors, url)

	// only generate preview cards if we can get at least a title and a summary.
	// if there is no summary, a icon might also work
	if (!title) {
		return null
	}
	if (!summary && !icon_url) {
		return null
	}

	return {
		title,
		summary: summary || title,
		icon_url,
		url
	}
}

export async function createFromUrl(client, other, url) {
	const data = await loadFromUrl(url)
	if (!data) {
		return null
	}

	data.other_id = other.id

	return await insertObject(client, 'other_preview', data)
}

export async function createFromLinks(client, other, links) {
	for (const link of links) {
		try {
			const preview = await createFromUrl(client, other, link)
			if (preview) {
				return preview
			}
		} catch (err) {
			logger.error(`could not create preview: ${err}`)
		}
	}

	return null
}
