import { insertObject, deleteObject, select, selectRow, selectPaginated } from '#lib/server/db'
import { extractFeatures, parseMarkdown } from '#lib/server/models/content'
import * as Othler from '#lib/server/models/othler'
import * as Preview from '#lib/server/models/preview'
import * as Embed from '#lib/server/models/embed'

export async function createFromCsv(client, csvData) {
	// TODO: should be validate on import?
	return await create(client, csvData.user_id, csvData.text, [], {
		id: csvData.post_id,
		created_at: csvData.created
	})
}

export async function post(client, othlerId, text, files) {
	return await create(client, othlerId, text, files)
}

export async function reply(client, othlerId, inReplyTo, text, files) {
	return await create(client, othlerId, text, files, {
		in_reply_to: inReplyTo
	})
}

async function create(client, othlerId, text, files, data = {}) {
	const html = parseMarkdown(text)

	const other = await insertObject(client, 'other', {
		...data,
		othler_id: othlerId,
		text_markdown: text,
		text_html: html
	})

	const { hashtags, mentions, links } = extractFeatures(text)

	for (const hashtag of hashtags) {
		await insertObject(client, 'other_hashtags', {
			hashtag,
			other_id: other.id
		})
	}

	for (const mention of mentions) {
		const othlerId = await Othler.getIdByHandle(client, mention)
		if (!othlerId) {
			continue
		}

		await insertObject(client, 'other_mentions', {
			other_id: other.id,
			othler_id: othlerId
		})
	}

	await Preview.createFromLinks(client, other, links)

	for (const file of files) {
		const embed = await Embed.createFromFile(client, file)
		await insertObject(client, 'other_embed', {
			other_id: other.id,
			othler_id: othlerId,
			embed_id: embed.id
		})
	}

	return other
}

function getQuery(othlerId) {
	return {
		from: 'view_other',

		select: {
			liked_at: {
				subquery: {
					from: 'other_likes',

					noDefaultSelect: true,
					select: {
						created_at: true,
						'.parent_id': {
							table: 'view_other',
							column: 'id'
						}
					},
					where: {
						liked_by: othlerId,
						other_id: { '=': { $ref: '.parent_id' } }
					}
				}
			}
		}
	}
}

export async function getById(client, othlerId, otherId) {
	return await selectRow(client, { ...getQuery(othlerId), where: { id: otherId } })
}

export async function getLocalTimeline(client, selfId, limit, lastKey) {
	return await selectPaginated(client, lastKey, {
		...getQuery(selfId),
		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},
		limit
	})
}

export async function getTimeline(client, othlerId, limit, lastKey) {
	const baseQuery = getQuery(othlerId)

	return await selectPaginated(client, lastKey, {
		...baseQuery,

		select: {
			...baseQuery.select,
			'.following': {
				subquery: {
					from: 'othler_following',
					select: 'follows_id',
					where: { othler_id: othlerId }
				}
			},
			'.mentions': {
				subquery: {
					from: 'other_mentions',
					select: 'other_id',
					where: { othler_id: othlerId }
				}
			}
		},

		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},

		where: [
			{ othler_id: othlerId },
			{ othler_id: { IN: { $ref: '.following' } } },
			{ id: { IN: { $ref: '.mentions' } } }
		],

		limit
	})
}

export async function getOthlerPosts(client, selfId, othlerId, limit, lastKey) {
	return await selectPaginated(client, lastKey, {
		...getQuery(selfId),
		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},
		where: {
			othler_id: othlerId,
			in_reply_to_id: null
		},
		limit
	})
}

export async function getOthlerPostsAndReplies(client, selfId, othlerId, limit, lastKey) {
	return await selectPaginated(client, lastKey, {
		...getQuery(selfId),
		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},
		where: {
			othler_id: othlerId
		},
		limit
	})
}

export async function getOthlerMediaPosts(client, selfId, othlerId, limit, lastKey) {
	return await selectPaginated(client, lastKey, {
		...getQuery(selfId),
		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},
		where: {
			othler_id: othlerId,
			embed_count: { '>=': 1 }
		},
		limit
	})
}

export async function getReplies(client, othlerId, otherId, limit, lastKey) {
	return await selectPaginated(client, lastKey, {
		...getQuery(othlerId),
		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},
		where: {
			in_reply_to_id: otherId
		},
		limit
	})
}

export async function like(client, othlerId, otherId) {
	const result = await insertObject(
		client,
		'other_likes',
		{
			other_id: otherId,
			liked_by: othlerId
		},
		{ onConflict: 'ignore' }
	)

	return !!result
}

export async function unlike(client, othlerId, otherId) {
	const rowCount = await deleteObject(client, 'other_likes', {
		other_id: otherId,
		liked_by: othlerId
	})

	return rowCount > 0
}

export async function repost(client, othlerId, otherId) {
	const result = await insertObject(
		client,
		'other',
		{
			othler_id: othlerId,
			text_markdown: '',
			text_html: '',
			reposts: otherId
		},
		{ onConflict: 'ignore' }
	)

	return !!result
}

export async function unrepost(client, othlerId, otherId) {
	const rowCount = await deleteObject(client, 'other', {
		othler_id: othlerId,
		reposts: otherId
	})

	return rowCount > 0
}

// Trends - could be in their own module, but then we would have to figure out
// how to expose getQuery in a sensible way

export async function getTrends(client, since, limit) {
	return await select(client, {
		from: 'other_hashtags',
		noDefaultSelect: true,
		select: {
			hashtag: true,
			count: { count: true }
		},
		groupBy: ['hashtag'],
		orderBy: { count: 'DESC' },
		where: {
			created_at: { '>=': { $value: since } }
		},
		limit
	})
}

export async function getByTag(client, othlerId, tag, limit, lastKey) {
	const baseQuery = getQuery(othlerId)

	return await selectPaginated(client, lastKey, {
		...baseQuery,

		select: {
			...baseQuery.select,

			'.hashtag': {
				table: 'other_hashtags',
				column: 'hashtag'
			}
		},

		join: [
			{
				required: true,
				from: 'other_hashtags',
				lhs: 'id',
				rhs: 'other_id'
			}
		],

		where: {
			'.hashtag': tag
		},

		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},

		limit
	})
}
