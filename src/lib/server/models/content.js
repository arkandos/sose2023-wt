import hljs from 'highlight.js'
import LinkifyIt from 'linkify-it'
import { marked } from 'marked'
import { markedHighlight } from 'marked-highlight'
import markedKatex from 'marked-katex-extension'
import markedLinkifyIt from 'marked-linkify-it'
import { markedTwemoji } from 'marked-twemoji'
import insane from 'insane'

const linkifySchemas = {
	'#': {
		validate(text, pos, self) {
			const tail = text.slice(pos)

			if (!self.re.hashtag) {
				self.re.hashtag = new RegExp('^(\\w+)(?=$|' + self.re.src_ZPCc + ')')
			}

			const match = tail.match(self.re.hashtag)
			if (!match) {
				return 0
			}

			return match[0].length
		},

		normalize(match) {
			match.url = '/tag/' + match.url.slice(1)
		}
	},

	'@': {
		validate(text, pos, self) {
			const tail = text.slice(pos)

			if (!self.re.mention) {
				self.re.mention = new RegExp('^(\\w+)(?=$|' + self.re.src_ZPCc + ')')
			}

			const match = tail.match(self.re.mention)
			if (!match) {
				return 0
			}

			return match[0].length
		},
		normalize(match) {
			match.url = '/othlers/' + match.url.slice(1)
		}
	}
}

marked.use({
	mangle: false,
	headerIds: false,
	breaks: true,
	gfm: true,

	renderer: {
		// render strong paragraph instead of a header element
		heading(html) {
			return `<p><strong>${html}</strong><p>`
		},
		// do never render inline html
		html(html) {
			return escapeHtml(html)
		},
		// render links as target=_blank if external
		link: renderImageOrLink,
		// we should not get inline images - use embeds instead.
		// if we still do, render them as links.
		image: renderImageOrLink
	}
})

marked.use({ extensions: [markedTwemoji] })
marked.use(markedLinkifyIt(linkifySchemas))
marked.use(
	markedHighlight({
		langPrefix: 'hljs language-',
		highlight(code, lang) {
			const language = hljs.getLanguage(lang) ? lang : 'plaintext'
			return hljs.highlight(code, { language }).value
		}
	})
)
marked.use(
	markedKatex({
		displayMode: true,
		output: 'html',
		throwOnError: false
	})
)

// we pass the output of marked into insane, which means inline html is already
// replaced by their literal representation, so we prevent XSS from that.
// what is left are attacks that are possible from markdown or katex,
// so mostly markdown links with javascript: stuff in them, etc.
const insaneOptions = {
	allowedTags: [
		'br',
		'hr',
		'img',
		'a',
		'b',
		'i',
		'strong',
		'em',
		'sub',
		'sup',
		'span',
		'p',
		'pre',
		'code',
		'blockquote',
		'ul',
		'ol',
		'li',
		'table',
		'thead',
		'tbody',
		'tr',
		'td',
		'th'
	],
	allowedAttributes: {
		img: ['class', 'src', 'alt'],
		a: ['href', 'title', 'target', 'rel'],
		span: ['class', 'style'],
		code: ['class']
	},
	allowedSchemes: ['http', 'https']
}

/**
 * Extracts used hashtags, mentions, and links using LinkifyIt.
 *
 * @param {string} markdown
 * @returns {{hashtags: string[], mentions: string[], links: string[]}}
 */
export function extractFeatures(markdown) {
	const linkify = new LinkifyIt(linkifySchemas)
	const matches = linkify.match(markdown) || []

	const hashtags = new Set()
	const mentions = new Set()

	// TODO: NOTE: If you use markdown links, those won't be extracted.
	const links = new Set()

	for (const match of matches) {
		if (match.schema === '#') {
			hashtags.add(match.raw.slice(1))
		} else if (match.schema === '@') {
			mentions.add(match.raw.slice(1))
		} else if (match.schema === 'https:') {
			links.add(match.url)
		}
	}

	return {
		hashtags: [...hashtags],
		mentions: [...mentions],
		links: [...links]
	}
}

/**
 * Parses and sanitizes the input markdown to HTML.
 *
 * @param {string} markdown
 * @returns {string}
 */
export function parseMarkdown(markdown) {
	// NOTE: we use marked.use() above, since some things only work doing that!
	// it means the options we pass to marked() are not quite obvious though!
	return insane(marked(markdown), insaneOptions, true)
}

const escapeTest = /[&<>"']/g
const escapeReplacements = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#39;'
}

function escapeHtml(html) {
	return html.replace(escapeTest, (ch) => escapeReplacements[ch])
}

function renderImageOrLink(href, title, text) {
	if (!href) {
		return text
	}
	// as far as I can tell from the marked source,
	// title is already escaped. Since we fallback to text here, we need to escape ourselves
	if (!title) {
		title = escapeHtml(text)
	}

	if (href.startsWith('/')) {
		return `<a href="${href}" title="${title}">${text || href}</a>`
	} else {
		return `<a href="${href}" title="${title}" rel="noreferrer noopen nofollow" target="_blank">${
			text || href
		}</a>`
	}
}
