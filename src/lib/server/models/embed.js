import { readFile, stat } from 'node:fs/promises'
import path from 'node:path'
import mime from 'mime'
import { selectRow, insertObject } from '#lib/server/db'

export async function createLocal(client, filename) {
	const filepath = path.join(process.env.LOCAL_EMBED_DIR, filename)
	const stats = await stat(filepath)

	return await insertObject(client, 'embed', {
		name: filename,
		local: true,
		size: stats.size,
		mime: mime.getType(filename),
		data: null
	})
}

export async function createFromFile(client, file) {
	const arrayBuffer = await file.arrayBuffer()

	return await insertObject(client, 'embed', {
		name: file.name,
		local: false,
		size: arrayBuffer.byteLength,
		mime: mime.getType(file.name),
		data: Buffer.from(arrayBuffer)
	})
}

export async function getById(client, id) {
	const embed = await selectRow(client, { from: 'embed', where: { id } })
	if (embed.local) {
		embed.data = await readFile(path.join(process.env.LOCAL_EMBED_DIR, embed.name))
	}

	return embed
}
