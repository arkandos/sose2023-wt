import bcrypt from 'bcrypt'
import {
	selectRow,
	selectValue,
	selectPaginated,
	insertObject,
	deleteObject,
	updateObject
} from '#lib/server/db'
import { parseMarkdown } from '#lib/server/models/content'
import * as Embed from '#lib/server/models/embed'

const saltRounds = 10

export async function createFromCsv(client, csvData) {
	// TODO: Should we validate on import?

	let avatar = null
	if (csvData.profile_pic) {
		avatar = await Embed.createLocal(client, csvData.profile_pic + '.png')
	}

	const passwordHash = await bcrypt.hash(csvData.password, saltRounds)

	return await insertObject(client, 'othler', {
		id: csvData.user_id,
		handle: csvData.name,
		name: csvData.name,
		password_hash: passwordHash,
		birthday: csvData.birthday,
		avatar: avatar?.id,
		bio_markdown: csvData.bio_text,
		bio_html: parseMarkdown(csvData.bio_text),
		created_at: csvData.created
	})
}

export async function register(client, { handle, name, password }) {
	if (password.length < 8) {
		throw new Error('password has to be at least 8 characters long!')
	}

	const passwordHash = await bcrypt.hash(password, saltRounds)

	return await insertObject(client, 'othler', {
		handle,
		name,
		password_hash: passwordHash
	})
}

export async function login(client, handle, password) {
	if (typeof handle !== 'string' || typeof password !== 'string') {
		return false
	}
	if (!handle || !password) {
		return false
	}

	const passwordHash = await selectValue(client, {
		select: 'password_hash',
		from: 'othler',
		where: { handle }
	})
	if (!passwordHash) {
		return false
	}

	return await bcrypt.compare(password, passwordHash)
}

export async function getIdByHandle(client, handle) {
	return await selectValue(client, {
		from: 'othler',
		select: 'id',
		where: { handle }
	})
}
export async function getNameByHandle(client, handle) {
	return await selectValue(client, {
		from: 'othler',
		select: 'name',
		where: { handle }
	})
}

function getQuery(othlerId) {
	return {
		from: 'view_othler',
		select: {
			followed_at: {
				subquery: {
					from: 'othler_following',
					noDefaultSelect: true,
					select: {
						created_at: true,
						'.parent_id': {
							table: 'view_othler',
							column: 'id'
						}
					},
					where: {
						othler_id: othlerId,
						follows_id: { '=': { $ref: '.parent_id' } }
					}
				}
			}
		}
	}
}

export async function getSelf(client, id) {
	return await selectRow(client, { from: 'view_othler', where: { id } })
}

export async function getByHandle(client, othlerId, handle) {
	return await selectRow(client, { ...getQuery(othlerId), where: { handle } })
}

export async function getRegistered(client, othlerId, limit, lastKey) {
	return await selectPaginated(client, lastKey, {
		...getQuery(othlerId),
		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},
		limit
	})
}

export async function getFollowing(client, othlerId, followerId, limit, lastKey) {
	const baseQuery = getQuery(othlerId)
	return await selectPaginated(client, lastKey, {
		...baseQuery,

		select: {
			...baseQuery.select,
			'.follower_id': {
				table: 'othler_following',
				column: 'othler_id'
			}
		},

		join: [
			{
				required: true,
				from: 'othler_following',
				lhs: 'id',
				rhs: 'follows_id'
			}
		],

		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},

		where: {
			'.follower_id': followerId
		},

		limit
	})
}

export async function getFollowers(client, othlerId, followingId, limit, lastKey) {
	const baseQuery = getQuery(othlerId)
	return await selectPaginated(client, lastKey, {
		...baseQuery,

		select: {
			...baseQuery.select,
			'.following_id': {
				table: 'othler_following',
				column: 'follows_id'
			}
		},

		join: [
			{
				required: true,
				from: 'othler_following',
				lhs: 'id',
				rhs: 'othler_id'
			}
		],

		orderBy: {
			created_at: 'DESC',
			id: 'DESC'
		},

		where: {
			'.following_id': followingId
		},

		limit
	})
}

export async function follow(client, followerId, followingId) {
	const result = await insertObject(
		client,
		'othler_following',
		{
			othler_id: followerId,
			follows_id: followingId
		},
		{ onConflict: 'ignore' }
	)

	return !!result
}

export async function unfollow(client, followerId, followingId) {
	const rowCount = await deleteObject(client, 'othler_following', {
		othler_id: followerId,
		follows_id: followingId
	})

	return rowCount > 0
}

export async function update(client, othlerId, { name, birthday, bio_markdown, avatar, banner, fields }) {
	// undefined -> no change (forward to updateObject which behaves the same)
	// file -> update
	// anything else -> remove

	let newAvatar
	if (avatar instanceof File) {
		const embed = await Embed.createFromFile(client, avatar)
		newAvatar = embed.id
	} else if (typeof avatar !== 'undefined') {
		newAvatar = null
	}

	let newBanner
	if (banner instanceof File) {
		const embed = await Embed.createFromFile(client, banner)
		newBanner = embed.id
	} else if (typeof banner !== 'undefined') {
		newBanner = null
	}

	const result = await updateObject(
		client,
		'othler',
		{ id: othlerId },
		{
			name,
			birthday,
			bio_markdown,
			bio_html: parseMarkdown(bio_markdown),
			avatar: newAvatar,
			banner: newBanner
		}
	)

	await deleteObject(client, 'othler_fields', { othler_id: othlerId })

	for (let i = 0; i < fields.length; ++i) {
		await insertObject(client, 'othler_fields', {
			othler_id: othlerId,
			position: i,
			name: fields[i].name,
			value: fields[i].value
		})
	}

	return result
}
