import { json, error } from '@sveltejs/kit'
import { getIdByHandle } from '#lib/server/models/othler'

export function withHandle(resolve) {
	return async function (event) {
		const othlerId = await getIdByHandle(null, event.params.handle)
		if (!othlerId) {
			throw error(404)
		}

		event.locals.handleId = othlerId

		return resolve(event)
	}
}

export function getOthlerPaginated(getPaginated) {
	return async function GET({ locals, params, url }) {
		const { othler: self } = locals
		const { handle } = params

		let othlerId = null
		if (params.handle) {
			othlerId = await getIdByHandle(null, handle)
			if (!othlerId) {
				throw error(404)
			}
		}

		const lastKey = url.searchParams.size ? Object.fromEntries(url.searchParams) : null

		const { items, next } = await getPaginated(null, self.id, othlerId, 10, lastKey)

		return json({ items, next })
	}
}
