import path from 'node:path'
import pg from 'pg'

import logger from '#lib/server/logger'
const { Pool, escapeIdentifier, escapeLiteral } = pg

const pool = new Pool()

/**
 * Closes all database connections in the internal pool.
 * On SIGTERM or SIGINT, all connections are automatically closed.
 */
export async function disconnect() {
	await pool.end()
}

/**
 * Applies all migrations in the top-level ./migrations/ dir.
 *
 * Migrations are SQL files that are executed sequentially on startup (sorted by their name).
 *
 * If a migration throws an error, the process exits. Migrations are always executed
 * in the context of a transaction, making each of them a "all-or-nothing" operation.
 *
 * Successfully applied migrations are also stored in a table "migrations".
 * Migrations are only ever executed once, by checking against this table (not thread-safe).
 */
export async function applyMigrations(migrations) {
	await query(
		pool,
		`CREATE TABLE IF NOT EXISTS migrations (
        filename VARCHAR(255) PRIMARY KEY,
        applied TIMESTAMP,
        sql TEXT
    )`
	)

	const appliedMigrationNames = new Set(await queryColumn(pool, 'SELECT filename FROM migrations'))

	for (const [filepath, sql] of Object.entries(migrations)) {
		const filename = path.basename(filepath)
		if (appliedMigrationNames.has(filename)) {
			logger.debug(`skipping migration ${filename}, already applied`)
			continue
		}

		const now = new Date()
		logger.info('applying migration ' + filename)

		try {
			await useTransaction(async (client) => {
				await query(client, sql)
				await insertObject(client, 'migrations', {
					applied: now,
					filename,
					sql
				})
			})
		} catch (err) {
			logger.error(`applying migration ${filename}: ${err}`)
			// we cannot start the server if we cannot apply all migrations.
			process.exit(1)
		}
	}
}

/**
 * use a connection from the pool, releasing the connection after the promise resolves or rejects.
 *
 * @param {(client: Client) => unknown} callback
 */
export async function useClient(callback) {
	const client = await pool.connect()
	try {
		return await callback(client)
	} finally {
		client.release()
	}
}

/**
 * use a transaction from the pool,
 * commiting the transaction after the promise resolves, or rolling back after the promise rejects.
 *
 * @param {(client: Client) => unknown} callback
 */
export async function useTransaction(callback) {
	const client = await pool.connect()
	try {
		await query(client, 'BEGIN')
		const result = await callback(client)
		await query(client, 'COMMIT')
		return result
	} catch (err) {
		await query(client, 'ROLLBACK')
		throw err
	} finally {
		client.release()
	}
}

/**
 * Internal low-level wrapper around client.query, adding logging.
 */
function queryRaw(client, queryStringOrObject, values) {
	if (!client) client = pool

	if (typeof queryStringOrObject === 'string') {
		logger.debug(`SQL: ${queryStringOrObject}; $ = ${JSON.stringify(values)}`)
	} else {
		logger.debug(
			`SQL: ${queryStringOrObject.text}; $ = ${JSON.stringify(queryStringOrObject.values)}`
		)
	}

	return client.query(queryStringOrObject, values)
}

/**
 * Query the DB, returning the rows.
 *
 * All other functions in the module are built upon this function, making
 * this a good place to put logging or breakpoints.
 *
 * @param {Client|null} client
 * @param {string|QueryConfig} queryStringOrObject
 * @param {unknown[]} values
 * @returns {Promise<unknown[]>}
 */
export async function query(client, queryStringOrObject, values) {
	const { rows } = await queryRaw(client, queryStringOrObject, values)
	return rows
}

/**
 * Query a single row from the database.
 * If the query returns multiple rows, additional data is thrown away.
 * Returning multiple rows is not an error.
 *
 * @param {Client|null} client
 * @param {string|QueryConfig} queryStringOrObject
 * @param {unknown[]} values
 * @returns {Promise<unknown>}
 */
export async function queryRow(client, queryStringOrObject, values) {
	const [first] = await query(client, queryStringOrObject, values)
	return first
}

/**
 * Query the values from a single (the first) column.
 * If the query returns multiple columns, additional data is thrown away.
 * Returning multiple columns is not an error.
 *
 * @param {Client|null} client
 * @param {string} queryString
 * @param {unknown[]} values
 * @returns {Promise<unknown[]>}
 */
export async function queryColumn(client, queryString, values) {
	const result = await query(client, {
		text: queryString,
		values: values,
		rowMode: 'array'
	})

	return result.map((r) => r[0])
}

/**
 * Query a single value from the database.
 *
 * If the query returns multiple columns or rows, only the first value will be
 * returned. A query returning additional data is not an error.
 *
 * @param {Client|null} client
 * @param {string} queryString
 * @param {unknown[]} values
 * @returns {Promise<unknown>}
 */
export async function queryValue(client, queryString, values) {
	const result = await queryColumn(client, queryString, values)
	if (result.length) {
		return result[0]
	} else {
		return null
	}
}

/**
 * insert a row using a row object, similar to the ones returned by queryRow.
 * The keys of the object need to match the column names.
 *
 * The inserted row is returned as it appears in the database, including all
 * additional default columns not given in the row object.
 *
 * @param {Client|null} client
 * @param {string} tableName
 * @param {Record<string,unknown>} object
 * @param {'error'|'ignore'} options.onConflict If set to 'ignore', return null instead of the inserted row on conflict
 * @returns {Promise<unknown>}
 */
export async function insertObject(client, tableName, object, options = {}) {
	tableName = escapeIdentifier(tableName)
	const columns = objKeys(object).map((key) => escapeIdentifier(key))
	const values = columns.map((_, i) => `$${i + 1}`)

	const onConflict = options.onConflict === 'ignore' ? 'ON CONFLICT DO NOTHING' : ''

	const queryString = `INSERT INTO ${tableName} (${columns}) VALUES (${values}) ${onConflict} RETURNING *`

	return await queryRow(client, queryString, objValues(object))
}

export async function updateObject(client, tableName, filterObject, updateObject) {
	tableName = escapeIdentifier(tableName)

	const values = []
	const addValue = (val) => {
		values.push(val)
		return '$' + values.length
	}

	const whereClause = objEntries(filterObject)
		.map(([col, val]) => `${escapeIdentifier(col)} = ${addValue(val)}`)
		.join(' AND ')

	const setClause = objEntries(updateObject)
		.map(([col, val]) => `${escapeIdentifier(col)} = ${addValue(val)}`)
		.join(', ')

	const queryString = `UPDATE ${tableName} SET ${setClause} WHERE ${whereClause} RETURNING *`

	return await queryRow(client, queryString, values)
}

/**
 * delete, using a partial row object.
 * All rows matching that object (having equal columns as the fields provided)
 * will be deleted.
 *
 * @param {Client|null} client
 * @param {string} tableName
 * @param {Record<string,unknown>} object
 * @returns {Promise<number>} the number of deleted rows.
 */
export async function deleteObject(client, tableName, object) {
	tableName = escapeIdentifier(tableName)

	const whereClause = objKeys(object)
		.map((col, i) => `${escapeIdentifier(col)} = $${i + 1}`)
		.join(' AND ')

	const values = objValues(object)

	const queryString = `DELETE FROM ${tableName} WHERE ${whereClause}`

	const { rowCount } = await queryRaw(client, queryString, values)
	return rowCount
}

/**
 * When inserting rows using explicit ids into a column with an auto-increment id sequence,
 * the sequence will become de-synced with the data, returning ids that already exist
 * in the database again.
 *
 * This function runs a query to fix this, setting the id sequence to the maximum
 * id found in the data.
 *
 * @param {Client|null} client
 * @param {string} tableName
 * @param {string?} idColumn defaults to "id"
 */
export async function fixAutoIncrement(client, tableName, idColumn = 'id') {
	const seqName = `${tableName}_${idColumn}_seq`

	const queryString = `SELECT SETVAL(${escapeLiteral(seqName)},
        (SELECT MAX(${escapeIdentifier(idColumn)}) FROM ${escapeIdentifier(tableName)}));`

	await query(client, queryString)
}

/**
 * Truncates a database table, removing all rows.
 * Makes sure that all triggers are run properly, and that the
 * id sequence is restarted afterwards.
 *
 * @param {Client|null} client
 * @param {string} tableName
 */
export async function truncate(client, tableName) {
	tableName = escapeIdentifier(tableName)

	// first delete to make sure all triggers run.
	await query(client, `DELETE FROM ${tableName}`)
	await query(client, `TRUNCATE ${tableName} RESTART IDENTITY CASCADE`)
}

/**
 * Queries should be manipulatible as data.
 * Introducing a buggy, insecure, ad-hoc, undocumented query builder KEKW
 *
 * But it is so deceptively simple and powerful, I''ve impressed myself!
 *
 * @example
 *
 * select({
    from: 'othler',
    alias: 'o',

    join: [
        { from: 'othler_following', alias: 'f', lhs: 'o.id', rhs: 'f.othler_id', required: true }
    ],

    where: {
        created_at: { '<=': new Date() },
        id: { '<=': 105 }
    },

    orderBy: {
        created_by: 'DESC',
        id: 'DESC'
    },

    limit: 10,
    offset: 0
    })
 *
 *
 * @param {*} spec
 * @returns {{ sql: string, values: unknown[] }} an object with sql and value props
 */
export function buildSelectQuery(spec, variablesToSkip = 0) {
	if (typeof spec.select === 'string') {
		spec.noDefaultSelect = true
		spec.select = { [spec.select]: true }
	}

	const parts = []
	const values = []

	const fromTable = escapeIdentifier(spec.alias || spec.from)
	const getColumn = (aliasName, defaultTable) => {
		if (!spec.select || !(aliasName in spec.select)) {
			if (defaultTable) {
				return escapeIdentifier(defaultTable) + '.' + escapeIdentifier(aliasName)
			} else {
				return fromTable + '.' + escapeIdentifier(aliasName)
			}
		}

		const def = spec.select[aliasName]
		if (def === true) {
			return fromTable + '.' + escapeIdentifier(aliasName)
		} else if (typeof def === 'string') {
			return fromTable + '.' + escapeIdentifier(def)
		} else if (def.table) {
			return escapeIdentifier(def.table) + '.' + escapeIdentifier(def.column)
		} else if (def.column) {
			return fromTable + '.' + escapeIdentifier(def.column)
		} else if (def.count) {
			return 'COUNT(*)'
		} else if (def.subquery) {
			// NOTE: this is sometimes not allowed, but we do it anyways and
			// hope SQL will give us a good error?
			const { sql: subSql, values: subValues } = buildSelectQuery(def.subquery, values.length)
			values.push(...subValues)
			return `(${subSql})`
		}
	}

	const addValue = (val) => {
		values.push(val)
		return '$' + (values.length + variablesToSkip)
	}

	const buildValue = (val, defaultIsColumn = false) => {
		if (val === null) {
			return 'NULL'
		} else if (typeof val === 'object' && !Array.isArray(val)) {
			if (val.$value) {
				return addValue(val.$value)
			} else if (val.$ref) {
				return getColumn(val.$ref)
			}
		} else if (typeof val === 'string' && defaultIsColumn) {
			return getColumn(val)
		} else {
			return addValue(val)
		}
	}

	parts.push('SELECT')

	const selectParts = []
	if (!spec.noDefaultSelect) {
		selectParts.push(fromTable + '.*')
	}

	if (spec.select) {
		for (const [aliasName, columnDef] of objEntries(spec.select)) {
			if (aliasName[0] === '.') {
				continue
			}
			if (!columnDef) {
				continue
			}

			if (columnDef === true) {
				selectParts.push(getColumn(aliasName))
			} else {
				selectParts.push(getColumn(aliasName) + ' AS ' + escapeIdentifier(aliasName))
			}
		}
	}

	parts.push(selectParts.join(','))

	parts.push('FROM')
	if (typeof spec.from === 'object') {
		const { sql: subSql, values: subValues } = buildSelectQuery(spec.from, values.length)
		values.push(...subValues)
		parts.push(`(${subSql})`)
	} else {
		parts.push(escapeIdentifier(spec.from))
	}

	if (spec.alias) {
		parts.push('AS')
		parts.push(escapeIdentifier(spec.alias))
	}

	if (spec.join) {
		for (const join of spec.join) {
			if (join.required) {
				parts.push('INNER JOIN')
			} else {
				parts.push('LEFT JOIN')
			}

			parts.push(escapeIdentifier(join.from))
			if (join.alias) {
				parts.push('AS')
				parts.push(escapeIdentifier(join.alias))
			}

			parts.push('ON')
			parts.push(getColumn(join.lhs))
			parts.push('=')
			parts.push(getColumn(join.rhs, join.alias || join.from))
		}
	}

	const where = Array.isArray(spec.where) ? spec.where : [spec.where]

	const whereParts = []
	for (const whereObj of where) {
		if (!whereObj) {
			continue
		}

		const andParts = []
		for (const [column, conditionSpec] of objEntries(whereObj)) {
			const colEscaped = getColumn(column)
			if (Array.isArray(conditionSpec)) {
				andParts.push(`${colEscaped} IN (${addValue(conditionSpec)})`)
			} else if (conditionSpec === null) {
				andParts.push(`${colEscaped} IS NULL`)
			} else if (typeof conditionSpec === 'object') {
				for (const [op, valueSpec] of objEntries(conditionSpec)) {
					andParts.push(`${colEscaped} ${op} ${buildValue(valueSpec)}`)
				}
			} else {
				andParts.push(`${colEscaped} = ${addValue(conditionSpec)}`)
			}
		}

		if (andParts.length) {
			whereParts.push('(' + andParts.join(') AND (') + ')')
		}
	}

	if (whereParts.length) {
		parts.push('WHERE')
		parts.push('(' + whereParts.join(') OR (') + ')')
	}

	if (spec.groupBy && spec.groupBy.length) {
		parts.push('GROUP BY')
		parts.push(spec.groupBy.map((col) => getColumn(col)).join(','))
	}

	if (spec.orderBy) {
		const orderParts = []
		for (const [column, order] of objEntries(spec.orderBy)) {
			orderParts.push(getColumn(column) + ' ' + order)
		}
		if (orderParts.length) {
			parts.push('ORDER BY')
			parts.push(orderParts.join(','))
		}
	}

	if (spec.limit) {
		parts.push('LIMIT')
		parts.push(addValue(spec.limit))
	}
	if (spec.offset) {
		parts.push('OFFSET')
		parts.push(addValue(spec.offset))
	}

	const sql = parts.join(' ')

	return { sql, values }
}

/**
 * Use a buildSelectQuery to query multiple rows.
 *
 * @param {Client|null} client
 * @param {*} spec
 * @returns {Promise<unknown[]>}
 */
export async function select(client, spec) {
	const { sql, values } = buildSelectQuery(spec)
	return await query(client, sql, values)
}

/**
 * Use a buildSelectQuery to get a single row.
 * Similar to queryRow, returning multiple rows from the database is not an error,
 * additional data will be discarded.
 *
 * @param {Client|null} client
 * @param {*} spec
 * @returns {Promise<unknown>}
 */
export async function selectRow(client, spec) {
	const { sql, values } = buildSelectQuery(spec)
	return await queryRow(client, sql, values)
}

/**
 * Use a buildSelectQuery to get a single column.
 * Similar to queryColumn, returning multiple columns is not an error.
 *
 * @param {Client|null} client
 * @param {*} spec
 * @returns {Promise<unknown[]>}
 */
export async function selectColumn(client, spec) {
	const { sql, values } = buildSelectQuery(spec)
	return await queryColumn(client, sql, values)
}

/**
 * Use a buildSelectQuery to get a single value.
 * Similar to queryValue, returning multiple values is not an error.
 *
 * @param {Client|null} client
 * @param {*} spec
 * @returns {Promise<unknown>}
 */
export async function selectValue(client, spec) {
	const { sql, values } = buildSelectQuery(spec)
	return await queryValue(client, sql, values)
}

/**
 * Build a paginated query that uses a key based on the sort order.
 *
 * This does not allow jumping to a "page", but allows for fast pagination in
 * "infinite scroll" situations, since we can use an index lookup to skip ahead
 * to the right place.
 *
 * The sort order in the spec has to be given explicitely,
 * allowing the construction of a `key`.
 * All columns in the sort order need to be selected, and are used to extract
 * that key from the last returned row.
 *
 * This makes it possible to page through all the data, without using an
 * ever-increaseing offset.
 *
 * @param {Client|null} client
 * @param {*} lastKey
 * @param {*} spec Spec will be modified using the lastKey, so you need to make sure to pass in a fresh object every time!
 * @returns {Promise<{ items: unknown[], next: unknown }>} an object with the `items`, and the `next` key.
 */
export async function selectPaginated(client, lastKey, spec) {
	if (lastKey) {
		if (!Array.isArray(spec.where)) {
			spec.where = [spec.where ?? {}]
		}

		// extend every OR clause with the lastKey filter
		for (const andPart of spec.where) {
			for (const [col, lastVal] of objEntries(lastKey)) {
				if (!(col in andPart)) {
					andPart[col] = {}
				}

				// having the column in the orderBy spec is required.
				const op = spec.orderBy[col] === 'ASC' ? '>=' : '<='

				andPart[col][op] = lastVal
			}
		}

		// since we compare with <= or >=, we skip the one that is "equal"
		spec.offset = 1
	} else {
		spec.offset = 0
	}

	const items = await select(client, spec)
	if (!items.length) {
		return { items, next: null }
	}

	const next = {}
	for (const col of objKeys(spec.orderBy)) {
		next[col] = items[items.length - 1][col]
	}

	return { items, next }
}

///
/// UTILS
///

function isUndefined(v) {
	return typeof v === 'undefined'
}

function objEntries(obj) {
	return Object.entries(obj).filter(([, val]) => !isUndefined(val))
}

function objKeys(obj) {
	return objEntries(obj).map((x) => x[0])
}

function objValues(obj) {
	return objEntries(obj).map((x) => x[1])
}
