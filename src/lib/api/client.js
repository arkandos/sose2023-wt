// roughly inspired by mande (https://github.com/posva/mande)

export class ClientError extends Error {}

class Client {
	constructor(options = {}) {
		this.instanceOptions = options
	}

	get(url, options = {}) {
		return this.request('GET', url, undefined, options)
	}

	delete(url, options = {}) {
		return this.request('DELETE', url, undefined, options)
	}

	post(url, data, options = {}) {
		return this.request('POST', url, data, options)
	}

	put(url, data, options = {}) {
		return this.request('PUT', url, data, options)
	}

	patch(url, data, options = {}) {
		return this.request('PATCH', url, data, options)
	}

	async request(method, url, body, options = {}) {
		const isJsonBody = !(
			body instanceof FormData ||
			body instanceof Blob ||
			body instanceof ArrayBuffer ||
			body instanceof ReadableStream ||
			typeof body === 'string'
		)

		options = {
			...this.constructor.defaults,
			...this.instanceOptions,
			...options,

			method,

			body: typeof body !== 'undefined' && isJsonBody ? JSON.stringify(body) : body,

			headers: {
				...this.constructor.defaults.headers,
				...(isJsonBody ? { 'Content-Type': 'application/json' } : null),
				...this.instanceOptions.headers,
				...options.headers
			},

			query: {
				...this.constructor.defaults.query,
				...this.instanceOptions.query,
				...options.query
			}
		}

		url = joinUrl(options.baseUrl, url)
		const searchParams = new URLSearchParams(options.query)
		if (searchParams.size) {
			url += '?' + searchParams.toString()
		}

		const response = await options.fetch(url, options)
		const responseBody = await response[options.responseAs]().catch(() => null)
		if (response.status < 200 || response.status >= 300) {
			const err = new ClientError(response.statusText)
			err.status = response.status
			err.response = response
			err.body = responseBody
			err.options = options

			await options.onError(err)
		}

		return await options.onResponse(responseBody, response, options)
	}
}

Client.defaults = {
	responseAs: 'json',
	headers: {
		Accept: 'application/json'
	},

	fetch: globalThis.fetch,

	onResponse(responseBody) {
		return responseBody
	},

	onError(error) {
		throw error
	}
}

export default function createClient(baseUrl = '/', instanceOptions = {}) {
	instanceOptions.baseUrl = baseUrl
	return new Client(instanceOptions)
}

function joinUrl(base, url) {
	base = base.replace(/\/+$/, '')
	url = url.replace(/^\/+/, '')

	if (base && url) {
		return `${base}/${url}`
	} else if (base) {
		return base
	} else {
		return url
	}
}
