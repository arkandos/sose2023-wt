import { error, redirect } from '@sveltejs/kit'
import { get } from 'svelte/store'
import { goto, invalidate } from '$app/navigation'
import { page } from '$app/stores'
import createClient from './client'

export default function createApiClient({ fetch } = {}) {
	fetch = fetch || globalThis.fetch
	const hasCustomFetch = fetch !== globalThis.fetch
	const isInLoadContext = hasCustomFetch

	return createClient('/api', {
		fetch,

		onResponse(body, response, options) {
			if (options.method !== 'HEAD' && options.method !== 'GET') {
				invalidate(response.url)
			}
			return body
		},

		onError(err) {
			// API returned an auth failure,
			// so we return back to the main page.
			if (err.status === 401) {
				if (isInLoadContext) {
					throw redirect(307, '/')
				}

				const url = get(page).url
				if (url.pathname !== '/') {
					goto('/')
				} else {
					throw err
				}
			} else if (isInLoadContext) {
				throw error(err.status, err.body)
			} else {
				throw err
			}
		}
	})
}
