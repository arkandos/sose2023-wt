export default function focus(node, enabled) {
	if (enabled) {
		requestAnimationFrame(() => node.focus())
	}

	return {
		update(newEnabled) {
			if (newEnabled && !enabled) {
				requestAnimationFrame(() => node.focus())
				enabled = newEnabled
			}
		}
	}
}
