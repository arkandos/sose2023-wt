export function shortCount(count) {
	const table = {
		M: 1000000,
		k: 1000
	}

	for (const [suffix, value] of Object.entries(table)) {
		if (count >= value) {
			const rounded = Math.round(count % (value / 10)) / 10
			return `${rounded}${suffix}`
		}
	}

	return `${count}`
}

const relativeTimeFormatter = new Intl.RelativeTimeFormat('de', {
	style: 'short',
	numeric: 'auto'
})

export function shortTimeAgo(date) {
	const table = [
		{
			multiplier: 1000,
			unit: 'second',
			threshold: 45
		},
		{
			multiplier: 60,
			unit: 'minute',
			threshold: 45
		},
		{
			multiplier: 60,
			unit: 'hour',
			threshold: 22
		},
		{
			multiplier: 24,
			unit: 'day',
			threshold: 5
		},
		{
			multiplier: 7,
			unit: 'week',
			threshold: 4
		},
		{
			multiplier: 4,
			unit: 'month',
			threshold: 6
		},
		{
			multiplier: 12,
			unit: 'year',
			threshold: null
		}
	]

	const diff = Date.parse(date) - Date.now()

	let value = diff
	let unit = 'milliseconds'
	for (const spec of table) {
		value = value / spec.multiplier
		unit = spec.unit

		if (!spec.threshold || Math.abs(value) < spec.threshold) {
			break
		}
	}

	if (Math.abs(value) < 1) {
		value = value > 0 ? 1 : -1
	} else {
		value = Math.round(value)
	}

	return relativeTimeFormatter.format(value, unit)
}

const absoluteTimeFormatter = new Intl.DateTimeFormat('de', {
	dateStyle: 'full',
	timeStyle: 'full'
})

export function longDateTime(date) {
	if (!(date instanceof Date)) {
		date = new Date(date)
	}

	return absoluteTimeFormatter.format(date)
}

const dateFormatter = new Intl.DateTimeFormat('de', {
	dateStyle: 'full'
})

export function longDate(date) {
	if (!(date instanceof Date)) {
		date = new Date(date)
	}

	return dateFormatter.format(date)
}
